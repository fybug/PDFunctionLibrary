/**
 * <h2>Java 拓展函数库.</h2>
 * 为 Java 增加算法库<br/>
 * 处理工具包，及语法糖函数<br/>
 * 增加数据类型以及注解类
 *
 * @author fybug
 * @version 0.0.4
 * @since JDK 1.8
 */
package fybug.nulll.pdfunctionlibrary;